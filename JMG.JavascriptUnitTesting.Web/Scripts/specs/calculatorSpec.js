﻿/// <reference path="../_references.js" />
/// <reference path="../jasmine-2.0.0/boot.js" />
/// <reference path="../jasmine-2.0.0/console.js" />
/// <reference path="../jasmine-2.0.0/jasmine-html.js" />
/// <reference path="../jasmine-2.0.0/jasmine.js" />
/// <reference path="../knockout-3.1.0.debug.js" />
/// <reference path="../apps/calculator.js" />

describe("When resources are included", function () {
    it("JMG.Apps.Calculator is defined", function () {
        expect(JMG.Apps.Calculator).toBeDefined();
    });
});

describe("Given a calculator instance", function () {
    var calc = new JMG.Apps.Calculator();
    
    it("then current number is set to zero", function () {
        expect(calc.currentNumber()).toEqual(0);
    });
});

describe("Given a calculator with current number set to 0", function () {
    var calc = null;
    beforeEach(function () {
        calc = new JMG.Apps.Calculator();
        calc.currentNumber(0);
    });

    it("when number is clicked, then it replaces 0 with the new number", function () {
        calc.numberClick(7);
        expect(calc.currentNumber()).toEqual(7);
    });
});

describe("Given a calculator with current number set to 3", function () {
    var calc = null;
    beforeEach(function () {
        calc = new JMG.Apps.Calculator();
        calc.currentNumber(3);
    });

    it("when number is clicked, then it concatenates the old number with the new number", function () {
        calc.numberClick(7);
        expect(calc.currentNumber()).toEqual(37);
    });
});

describe("Given a calculator with a current number set to 56", function () {
    var calc = null;
    calc = new JMG.Apps.Calculator();
    calc.currentNumber(56);

    it("clears the number out on clear click", function () {
        calc.clearClick();
        expect(calc.currentNumber()).toEqual(0);
    });
});

describe("Given a calculator with a current number set to 2", function () {
    var calc = null;
    calc = new JMG.Apps.Calculator();
    calc.currentNumber(2);

    describe("when addition is applied", function () {
        calc.operatorClick("+");
        it("then updates expression and clears number", function () {
            expect(calc.expression).toEqual("2 +");
        });
    });
});

describe("Given a calculator with the current expression set to '2 +' and current number set to 2 ", function () {
    var calc = null;
    calc = new JMG.Apps.Calculator();
    calc.expression = "2 + ";
    calc.currentNumber(2);

    describe("when total is clicked", function () {
        calc.totalClick();
        it("then evaluates the expression and sets the result", function () {
            expect(calc.currentNumber()).toEqual(4);
        });
    })
});