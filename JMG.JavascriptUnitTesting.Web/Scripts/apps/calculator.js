﻿/// <reference path="../_references.js" />

var JMG = JMG || {};
JMG.Apps = JMG.Apps || {};
JMG.Apps.Calculator = (function () {
    return function () {
        this.currentNumber = ko.observable(0);
        this.expression = "";
        this.concatNumber = function (newNumber) {
            var currentNumber;
            currentNumber = this.currentNumber();
            if (currentNumber == 0) {
                this.currentNumber(newNumber);
            } else {
                this.currentNumber(parseInt(currentNumber.toString() + newNumber.toString()));
            }
        };
        this.numberClick = function (number) {
            this.concatNumber(number);
        };

        this.operatorClick = function (operator) {
            var expression, currentNumber;
            currentNumber = this.currentNumber();
            if (currentNumber != 0) {
                expression = this.expression + " " + currentNumber + " " + operator;
                this.expression = expression.trim();
                this.currentNumber(0);
            }
        };

        this.totalClick = function () {
            var result, expression, currentNumber;
            currentNumber = this.currentNumber();
            expression = this.expression + " " + currentNumber;
            result = eval(expression);
            this.expression = "";
            this.currentNumber(result);
        };

        this.clearClick = function () {
            this.currentNumber(0);
            this.expression = "";
        };
    };
}());